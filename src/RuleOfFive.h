#pragma once

#include <iostream>

class RuleOfFive
{
public:
	RuleOfFive()
	{
		std::cout << "RuleOfFive Default ctor" << std::endl;
	}

	RuleOfFive(const RuleOfFive&)
	{
		std::cout << "RuleOfFive copy ctor" << std::endl;
	}

	RuleOfFive(RuleOfFive&&)
	{
		std::cout << "RuleOfFive move ctor" << std::endl;
	}

	RuleOfFive& operator=(const RuleOfFive&)
	{
		std::cout << "RuleOfFive copy asignment operator" << std::endl;
		return *this;
	}

	RuleOfFive& operator=(RuleOfFive&&)
	{
		std::cout << "RuleOfFive move asignment operator" << std::endl;
		return *this;
	}

	~RuleOfFive()
	{
		std::cout << "~RuleOfFive() called" << std::endl;
	}

public:
	static void Explain()
	{
		RuleOfFive r; // default constructor
		RuleOfFive tmp = r; // not a cpy assignment op. Copy ctor
		tmp = r; // This is a cpy asignment op.
		RuleOfFive tmp2(r); // cpy ctor
		RuleOfFive tmp3;
		tmp3 = std::move(r); // move asignment op.
		RuleOfFive tmp4(std::move(r)); // move consructor.
	}

};
