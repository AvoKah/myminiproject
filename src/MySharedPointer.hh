#pragma once

#include "MySharedPointer.h"

#include <ostream>
#include <memory>

//#define PRINT

namespace
{
// To handle base case of below recursive 
// Variadic function Template 
void print()
{
	std::cout << std::endl;
}

// Variadic function Template that takes  
// variable number of arguments and prints 
// all of them. 
template <typename T, typename... Types>
void print(T var1, Types... var2)
{
   std::cout << var1 << ", ";
   print(var2...);
}
}

namespace cstm
{
template <class C>
MySharedPointer<C>::MySharedPointer(C* ptr = nullptr) :
	mCounter(new long(1))
{
#ifdef PRINT
	std::cout << "MySharedPointer::MySharedPointer() called" << std::endl;
#endif
   if (ptr == nullptr)
      mObj_ptr = new C();
   else
      mObj_ptr = ptr;
}

template <class C>
template <typename... Types>
MySharedPointer<C>::MySharedPointer(Types... args) :
   mCounter(new long(1))
{
#ifdef PRINT
	std::cout << "MySharedPointer::MySharedPointer(Types... args) called" << std::endl;
	print(args...);
#endif
	mObj_ptr = new C(args...);
}

template <class C>
MySharedPointer<C>::MySharedPointer(const MySharedPointer& other)
{
#ifdef PRINT
	std::cout << "MySharedPointer(const MySharedPointer&) called" << std::endl;
#endif
	if (&other != this)
	{
		mObj_ptr = other.mObj_ptr;
		mCounter = other.mCounter;
		(*mCounter)++;
	}
}

template <class C>
MySharedPointer<C>& MySharedPointer<C>::operator=(const MySharedPointer& other)
{
#ifdef PRINT
	std::cout << "MySharedPointer operator=(const MySharedPointer&) called" << std::endl;
#endif
	if (&other != this)
	{
		mObj_ptr = other.mObj_ptr;
		mCounter = other.mCounter;
		(*mCounter)++;
	}
	return *this;
}

template<class C>
MySharedPointer<C>::operator bool() const
{
   return mObj_ptr != 0;
}

template<class C>
C& MySharedPointer<C>::operator*() const
{
#ifdef PRINT
	std::cout << "MySharedPointer::operator*()" << std::endl;
#endif
	return *mObj_ptr;
}

template<class C>
C* MySharedPointer<C>::operator->() const
{
   return mObj_ptr;
}

template<class C>
std::ostream& operator<<(std::ostream& os, const MySharedPointer<C>& sp)
{
   os << "Address pointed : " << sp.get();
   return os;
}

template<class C>
C* MySharedPointer<C>::get() const
{
   return mObj_ptr;
}

template<class C>
inline bool MySharedPointer<C>::unique() const
{
   return *mCounter == 1;
}

template<class C>
long MySharedPointer<C>::use_count() const
{
	return *mCounter;
}

template <class C>
MySharedPointer<C>::~MySharedPointer()
{
#ifdef PRINT
	std::cout << "MySharedPointer::~MySharedPointer()" << std::endl;
#endif
	if (*mCounter > 0)
	{
		--(*mCounter);
	}
	else
	{
      if (mObj_ptr != nullptr)
		   delete mObj_ptr;
	  delete mCounter;
	}
}

template <class C, typename... Types>
MySharedPointer<C> make_shared(Types... args)
{
	return MySharedPointer<C>(args...);
}
}