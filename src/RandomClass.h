#pragma once

#include <iostream>

/// Class to test the shared pointers
class RandomClass
{
public:
	RandomClass(const std::string& name, int age) : 
		mName(name),
		mAge(age)
	{
		std::cout << "Random class CONSTRUCTOR called" << std::endl;
	}

	RandomClass(const RandomClass&) = delete;
	RandomClass(RandomClass&&) = delete;
	RandomClass& operator=(const RandomClass&) = delete;
	RandomClass& operator=(RandomClass&&) = delete;

	~RandomClass()
	{
		std::cout << "~RandomClass() called" << std::endl;
	}

	void Display()
	{
		std::cout << "RandomClass\nName = " << mName.c_str() << " , age = " << mAge << std::endl;
	}

private:
	std::string mName;
	int mAge;
};

