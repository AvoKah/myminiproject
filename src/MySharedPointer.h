#pragma once

#include <iostream>

namespace cstm
{
template <class C>
class MySharedPointer
{
public:
   /// Default constructor.
   /// \param[in] ptr Ptr on the object. Default is nullptr.
   explicit MySharedPointer(C* ptr);

   /// Constructor with C's constructor arguments.
	template<typename ...Types>
	explicit MySharedPointer(Types ...args);

   /// Destructor.
	~MySharedPointer();

   /// Copy Constructor
	MySharedPointer(const MySharedPointer& other);

   /// TODO
   /// Move constructor
	MySharedPointer(MySharedPointer&& other);

   /// Copy asignment operator
	MySharedPointer& operator=(const MySharedPointer& other);

   /// TODO
   /// Move asignment operator
	MySharedPointer& operator=(MySharedPointer&& other);

   /// Verifies if *this handles an object
   /// \return true if *this handles an object (= if mObj_ptr != 0).
   operator bool() const;

   /// Dereferences the object.
   /// @{
   /// \return A reference to the object.
	C& operator*() const;
   /// \return A pointer to the object.
	C* operator->() const;
   /// @}

   /// Writes the pointer address in os.
   friend std::ostream& operator<<(std::ostream& os, const MySharedPointer<C>& sp);

   /// Verifies if *this precedes other.
   /// \param[in] other The smart pointer to compare.
   /// \return true if *this precedes other.
   bool owner_before(const MySharedPointer<C>& other) const;

   /// Replaces the object by ptr.
   /// \param[in] ptr A pointer to the new object. default is nullptr
   void reset(C* ptr);

   /// Swap the content of *this and other
   /// \param[in/out] other The smart pointer to trade content with.
   void swap(MySharedPointer<C>& other);

   /// Returns a pointer to the object handled.
   /// \return A pointer to the object.
   C* get() const;

   /// Verifies if the use count is 1
   /// \return True if the counter is 1.
   bool unique() const;

   /// Returns the number of shared pointer instances.
   /// \return The number of shared pointer instances.
	long use_count() const;

private:

   /// Pointer to the ref count.
	long* mCounter;

   /// Pointer to the object.
	C* mObj_ptr;
};

/// Constructs a shared pointer of type C
/// \param[in] The list of arguments that will construct an instance of C.
template <class C, typename... Types>
MySharedPointer<C> make_shared(Types... args);

}

#include "MySharedPointer.hh"