#include <iostream>

#include "RandomClass.h"
#include "MySharedPointer.h"

#include "RuleOfFive.h"

#include <assert.h>

//#define PRINT
#define MINE

template<class SmrtPtr>
void test()
{
	using namespace std;
	// ptr1 pointing to an integer. 
#ifndef MINE
	auto ptr1 = make_shared<int>(/*new int(151)*/);
#else
	auto ptr1 = cstm::make_shared<int>();
#endif
	cout << "--- Shared pointers ptr1 ---\n";
	cout << ptr1 << " Counter : " << ptr1.use_count() << std::endl;

	{
		// ptr2 pointing to same integer 
		// which ptr1 is pointing to 
		// Shared pointer reference counter 
		// should have increased now to 2. 
		SmrtPtr ptr2 = ptr1;
		cout << "--- Shared pointers ptr1, ptr2 ---\n";
		cout << ptr1 << " Counter : " << ptr1.use_count() << std::endl;
		cout << ptr2 << " Counter : " << ptr2.use_count() << std::endl;

		{
			// ptr3 pointing to same integer 
			// which ptr1 and ptr2 are pointing to. 
			// Shared pointer reference counter 
			// should have increased now to 3. 
			SmrtPtr ptr3(ptr2);
			cout << "--- Shared pointers ptr1, ptr2, ptr3 ---\n";
			cout << ptr1 << " Counter : " << ptr1.use_count() << std::endl;
			cout << ptr2 << " Counter : " << ptr2.use_count() << std::endl;
			cout << ptr3 << " Counter : " << ptr3.use_count() << std::endl;
		}

		// ptr3 is out of scope. 
		// It would have been destructed. 
		// So shared pointer reference counter 
		// should have decreased now to 2. 
		cout << "--- Shared pointers ptr1, ptr2 ---\n";
		cout << ptr1 << " Counter : " << ptr1.use_count() << std::endl;
		cout << ptr2 << " Counter : " << ptr2.use_count() << std::endl;
	}

	// ptr2 is out of scope. 
	// It would have been destructed. 
	// So shared pointer reference counter 
	// should have decreased now to 1. 
	cout << "--- Shared pointers ptr1 ---\n";
	cout << ptr1 << " Counter : " << ptr1.use_count() << std::endl;
}

void testSharedPointerObjectIncrement()
{
	using namespace cstm;
	const auto& myPointer = cstm::make_shared<int>(0);
	const auto& sharedPointer = std::make_shared<int>(0);

	// ref on the object
	auto myObjectRef = (*myPointer);
	auto& sharedPointerObjectRef = (*sharedPointer);
	
	// 0 -> 1
	++myObjectRef;
	++sharedPointerObjectRef;

	std::cout << "Incremented object = " << *myPointer << " vs " << *sharedPointer << std::endl;
	std::cout << "Ref count = " << myPointer.use_count() << " vs " << sharedPointer.use_count() << std::endl;
	assert(*myPointer == *sharedPointer);
}

void testSharedPointerOperator()
{
   using namespace cstm;
   MySharedPointer<int> p(new int(2));
   std::shared_ptr<int> sp(new int(2));
   const auto& myPointer = cstm::make_shared<RandomClass>("MySharedPointer", 4);
   const auto& sharedPointer = std::make_shared<RandomClass>("std::shared_ptr", 4);
   auto test = sharedPointer.operator->();
   myPointer->Display();
   sharedPointer->Display();
}

int main()
{
   testSharedPointerOperator();
#ifndef MINE
	test<std::shared_ptr<int>>();
#else
	//test <cstm::MySharedPointer<int>>();
#endif
	return 0;
}
